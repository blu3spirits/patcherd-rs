use std::process::Command;
use std::process::exit;
use std::fs;

#[macro_use]
extern crate serde_derive;
extern crate notify_rust;
extern crate serde_json;

use notify_rust::Notification;

#[derive(Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
struct Config {
    commands: Commands,
    threshold: i32,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
struct Commands {
    query: String,
    manager: String,
    update: String,
}

/* -----------------------------
 * main function
 * collect updates
 * build a notification message
 * show the action message
 * -----------------------------
 */

fn main() {
    let _config = read_config();
    let _threshold: i32 = _config.threshold;
    let _updates = get_updates(_config);
    if _updates >= _threshold {
        let _notification_message = build_notificion(_updates);
        show_action_message(_notification_message);
    } else {
        println!("Update threshold not reached, exiting");
        exit(0);
    }
}

/* -----------------------------
 * build notification
 * convert updates to string, from int
 * format the message
 * return the message
 * -----------------------------
 */

fn build_notificion(_updates: i32) -> String {
    let updates: String = _updates.to_string();
    let message = format!("You have {} updates!", updates);
    return message;
}

/* -----------------------------
 * get updates
 * run the system level package manager
 * with options to query updates
 * convert output to string
 * count the lines
 * return the number of updates
 * -----------------------------
 */

fn get_updates(_config: Config) -> i32 {
    let _raw_pacman_output = Command::new(_config.commands.manager)
        .arg(_config.commands.query)
        .output()
        .expect("Pacman query command failed!");
    let output = String::from_utf8_lossy(&_raw_pacman_output.stdout
        .as_slice());
    let _updates = output.lines().count() as i32;
    return _updates;
}

/* -----------------------------
 * show action message
 * display an action message that collects input
 * if the box is clicked update the system
 * if the box is NOT clicked, log that the message is closed and exit
 * -----------------------------
 */

fn show_action_message(_message: String) {
    Notification::new()
        .summary("PatcherD Notification")
        .body(&_message)
        .show()
        .unwrap();
}

/* -----------------------------
 * Read the configuration file from disk
 * place it into the Config structure
 * return config
 * -----------------------------
 */
fn read_config() -> Config {
    let _data = fs::read_to_string("./config.json")
        .expect("Unable to read configuration file");
    let config: Config = serde_json::from_str(&_data)
        .expect("JSON config was not formatted correctly");
    return config;
}
